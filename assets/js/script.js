/* Author: 

*/
var inputValue = document.querySelectorAll("input")[0], button = document.querySelectorAll("input")[1], letters = /^[A-Za-z\s]+$/;

inputValue.setAttribute("type", "text");
inputValue.placeholder = "Enter the City";
button.setAttribute("type", "submit");

//fetch api from openweathermap
button.addEventListener('click', function(e){
    e.preventDefault();
    if (inputValue.value.length == 0 || !letters.test(inputValue.value)) {
        alert('Wrong city name')
    }
    else {
        fetch('http://api.openweathermap.org/data/2.5/weather?q=' + inputValue.value + '&appid=25c893e6cbc877423eb1f6eec8955a6f')
        .then(response => response.json())
        .then(data => {
            var nameValue = data['name'],
            tempValue = (data['main']['temp'] - 273).toFixed(2),
            descValue = data['weather'][0]['description'];

            document.querySelectorAll(".display span")[0].innerHTML = "Name of City : " + nameValue;
            document.querySelectorAll(".display span")[1].innerHTML = "Temperature : " + tempValue + '&deg;C';
            document.querySelectorAll(".display span")[2].innerHTML = "Weather : " + descValue;
            document.querySelector("form").reset()
            document.querySelector("body").className = data['weather'][0]['main'];
        })
        .catch(err => alert('Wrong city name'));
    }
});
















